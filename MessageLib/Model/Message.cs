﻿using System.ComponentModel;

namespace MessageLib.Model
{
    public enum MessageEnum
    {
        [Description("安灯客户端的程序消息")]
        BoradCastClientMessage = 1,
        [Description("安灯消息")]
        SimulationMessage = 2,
        [Description("电视看板消息")]
        KanbanData = 3,
        [Description("Andon电视看板消息")]
        TvBoardMessage = 4,
        [Description("AVI实时生产消息")]
        AviProductionMessage = 5
    }

    public class Message
    {
        /// <summary>
        /// 消息主题
        /// </summary>
        public string TopicName { get; set; }

        public MessageEnum Type { get; set; }

        ///// <summary>
        ///// 消息等级
        ///// </summary>
        //public Qos Qos { get; set; }

        public Message()
        {

        }

        public Message(string topicName, MessageEnum type)
        {
            Type = type;
            TopicName = topicName;
            //  Qos = Qos.AtLeastOnce;
        }
        //public Message(string TopicName, Qos qos, MessageEnum type) : this(TopicName, type)
        //{
        //     Qos = Qos.AtLeastOnce;
        //}
    }

    public class AviProductionMessage : Message
    {
        public static readonly string MessageTopic = @"/AviProductionMessage/";

        /// <summary>
        /// 触发消息的工位
        /// </summary>
        public int Station { get; set; }
        /// <summary>
        /// 过点类型
        /// </summary>
        public int ThroughType { get; set; }

        /// <summary>
        /// 过点VIN码
        /// </summary>
        public string Vin { get; set; }

        public AviProductionMessage()
        {
        }

        public AviProductionMessage(string no) : base(MessageTopic + no, MessageEnum.AviProductionMessage)
        {

        }

        /// <summary>
        /// 
        /// </summary>t
        /// <param name="vstation">工位</param> 
        /// <param name="throughType">过点类型</param>
        /// <param name="vinCode">VIN码</param>
        public AviProductionMessage(string no, int vstation, int throughType, string vinCode) : base(MessageTopic + no, MessageEnum.AviProductionMessage)
        {
            Station = vstation;
            ThroughType = throughType;
            Vin = vinCode;
        }
    }

}
