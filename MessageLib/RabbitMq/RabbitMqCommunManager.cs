﻿using System; 
using MessageLib.Helper;
using MessageLib.Model;

namespace MessageLib.RabbitMq
{



    public class RabbitMqCommunManager : ICommunManager
    {
        private readonly string _clientId;
        private readonly string _no;
        public event Action<ConnectionState> StateUpdateEvent;
        //public event Action<KanbanData> KanbanEvent;
        //public event Action<BoradCastClientMessage> BroadCastClientEvent;
        //public event Action<SimulationMessage> SimulationEvent;
        //public event Action<TvBoardMessage> TvBoardMessageEvent;
       public event Action<AviProductionMessage> AviMessageEvent;
        //public event Action<AreaAudioReadyMessage> AudioMessageEvent;

        private readonly RabbitMqClient _commun;
        private readonly ISerialize _serialize = new MqttSerializeJson();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        /// <param name="clientId"></param>
        /// <param name="no">系统编号</param>
        /// <param name="logConfigPath">Log日志路径</param>
        public RabbitMqCommunManager(string ip, int port, string clientId, string no, string logConfigPath)
        {
            _clientId = clientId;
            _no = no;

            //_no = no;
            //_clientId = clientId;
            if (_commun == null)
            {
                var log = new LogInfo(logConfigPath, "");
                _commun = new RabbitMqClient(ip, port, log);

                _commun.PublishMessageEvent += _commun_PublishMessageEvent;

                _commun.StateUpdateEvent += _commun_StateUpdateEvent;

            }
        }

        private void _commun_StateUpdateEvent(ConnectionState state)
        {
            StateUpdateEvent?.Invoke(state);//连接修改事件 
        }

        public bool Push<T>(T message) where T : Message, new()
        {
            return _commun.Push(message.TopicName, message, _serialize);
        }

        public bool Push<T>(T message, int overdueTime) where T : Message, new()
        {
            return _commun.Push(message.TopicName, message, _serialize, overdueTime);
        }

        public bool Subscribe<T>(T message) where T : Message, new()
        {
            return _commun.Subscribe(message.TopicName);
        }

        public bool Subscribe<T>(T message, int overdueTime) where T : Message, new()
        {
            return _commun.Subscribe(message.TopicName, overdueTime);
        }

        public bool Connect()
        {
            return _commun.Connect();
        }

        private void _commun_PublishMessageEvent(string topicName, byte[] payload)
        {
            try
            {
                var message = _serialize.Deserialize<Message>(payload);
                switch (message.Type)
                {

                    //case MessageEnum.BoradCastClientMessage:
                    //    BroadCastClientEvent?.Invoke(_serialize.Deserialize<BoradCastClientMessage>(payload));
                    //    break;
                    //case MessageEnum.SimulationMessage:
                    //    SimulationMessage smMessage = _serialize.Deserialize<SimulationMessage>(payload);
                    //    smMessage.TopicName = message.TopicName;
                    //    SimulationEvent?.Invoke(smMessage);
                    //    break;
                    //case MessageEnum.KanbanData:
                    //    KanbanData kanbanData = _serialize.Deserialize<KanbanData>(payload);
                    //    KanbanEvent?.Invoke(kanbanData);
                    //    break;
                    //case MessageEnum.TvBoardMessage:
                    //    TvBoardMessage bordMessage = _serialize.Deserialize<TvBoardMessage>(payload);
                    //    TvBoardMessageEvent?.Invoke(bordMessage);
                    //    break;
                    case MessageEnum.AviProductionMessage:
                        AviProductionMessage avimMessage = _serialize.Deserialize<AviProductionMessage>(payload);
                        AviMessageEvent?.Invoke(avimMessage);
                        break;
                    //case MessageEnum.AreaAudioReadyMessage:
                    //    AreaAudioReadyMessage admMessage = _serialize.Deserialize<AreaAudioReadyMessage>(payload);
                    //    AudioMessageEvent?.Invoke(admMessage);
                    //    break;
                        //default:
                        //    throw new ArgumentOutOfRangeException();  // 可能数据丢失，反序列化出问题
                }
            }
            catch(Exception ex)
            {
            }
        }

    }
}
