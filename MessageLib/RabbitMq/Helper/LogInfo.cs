﻿using System.IO;
using System.Threading.Tasks;
using log4net; 

namespace MessageLib.Helper
{
    /// <summary>
    /// 日志
    /// </summary>
    public class LogInfo  
    {

        private readonly ILog _logger;
        public LogInfo(string filePath, string name)
        {
            FileInfo configFile = new FileInfo(filePath);
            log4net.Config.XmlConfigurator.Configure(configFile);
            _logger = LogManager.GetLogger(name);
        }
        public async void Debug(object message)
        { 
            await Task.Run(() =>
            {
                _logger.Debug(message);
            });
        }
        public async void Error(object message)
        { 
            await Task.Run(() =>
            {
                _logger.Error(message);
            });
        }

        //public async void Info(string title, MqttMessage message)
        //{
        //    await Task.Run(() =>
        //    {
        //        _logger.Info(title + " " + message.ToJson());
        //    });
        //}

    }
}
