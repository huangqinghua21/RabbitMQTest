﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Helper;
using MessageLib;
using MessageLib.Model;
using MessageLib.RabbitMq;

namespace Client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        // readonly CommunManager _commun = new CommunManager(new MqttSerializeJson());

        RabbitMqCommunManager _commun = new RabbitMqCommunManager("192.168.2.111", 5672, "", "sd", "log4net.config");
        private void push_btn_Click(object sender, EventArgs e)
        {

            //Task.Run(() =>
            //{
            //    while (true)
            //    {
            _commun.Push(new AviProductionMessage(no_txt.Text, 1, 2, "sdf"));
            Thread.Sleep(100);
            //    }

            //});
        }

        private void subscribe_btn_Click(object sender, EventArgs e)
        {
            _commun.Subscribe(new AviProductionMessage(no_txt.Text));
            _commun.AviMessageEvent += _commun_AviMessageEvent;
        }
        private void _commun_AviMessageEvent(AviProductionMessage obj)
        {
            LogEvent("接受", "队列消息:" + obj.TopicName);
        }
        private void LogEvent(string title, string data)
        {
            if (ckbShow.Checked)
            {

                if (txtInfo.InvokeRequired)
                {
                    //后端线程向主线程记日志
                    Invoke(new Action<string, string>(LogEvent), title, data);
                }
                else
                {
                    txtInfo.Text += title + data + Environment.NewLine;
                }

            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtInfo.Text = "";
        }
    }
}
