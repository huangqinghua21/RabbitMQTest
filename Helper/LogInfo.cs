﻿using System.IO;
using System.Threading.Tasks;
using log4net;
using MessageLib.Model;

namespace Helper
{
    public interface ILog
    {
        void Debug(object message);
        void Error(object message);
        void Info(string title, Message message);
    }
    /// <summary>
    /// 日志
    /// </summary>
    public class LogInfo :  ILog
    {

        private readonly log4net.ILog _logger;
        public LogInfo(string filePath, string name)
        {
            FileInfo configFile = new FileInfo(filePath);
            log4net.Config.XmlConfigurator.Configure(configFile);
            _logger = LogManager.GetLogger(name);
        }
        public async void Debug(object message)
        {
            await Task.Run(() =>
            {
                _logger.Debug(message);
            });
        }
        public async void Error(object message)
        {
            await Task.Run(() =>
            {
                _logger.Error(message);
            });
        }

        public async void Info(string title, Message message)
        {
            await Task.Run(() =>
            {
                _logger.Info(title + " " + message.ToJson());
            });
        }

    }
}
