﻿using System;
using MessageLib.Model;

namespace MessageLib
{
    public enum ConnectionState
    {
        /// <summary>
        /// 没有 连接
        /// </summary>
        None = 0,
        /// <summary>
        /// 断开的
        /// </summary>
        Disconnected = 1,
        /// <summary>
        ///   连接到代理的过程中
        /// </summary>
        Connecting = 2,
        /// <summary>
        /// 连接当前与代理连接
        /// </summary>
        Connected = 3
    }
    public interface ICommunManager
    {
        /// <summary>
        /// 连接修改事件
        /// </summary>
        event Action<ConnectionState> StateUpdateEvent;
        ///// <summary>
        ///// 看板消息 消息
        ///// </summary>
        //event Action<KanbanData> KanbanEvent;
        ///// <summary>
        ///// 语音广播消息
        ///// </summary>
        //event Action<BoradCastClientMessage> BroadCastClientEvent;
        ///// <summary>
        ///// 语音广播消息
        ///// </summary>
        //event Action<SimulationMessage> SimulationEvent;
        ///// <summary>
        ///// Andon电视看板更新显示的消息.
        ///// </summary>
        //event Action<TvBoardMessage> TvBoardMessageEvent;
        /// <summary>
        /// AVI实时生产消息
        /// </summary>
        event Action<AviProductionMessage> AviMessageEvent;
        ///// <summary>
        ///// 区域客户端播音准备就绪消息
        ///// </summary>
        //event Action<AreaAudioReadyMessage> AudioMessageEvent;
        /// <summary>
        /// 推送
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <returns></returns>
        bool Push<T>(T message) where T : Message, new();

        /// <summary>
        /// 推送
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <param name="overdueTime">过期时间 毫秒</param>
        /// <returns></returns>
        bool Push<T>(T message, int overdueTime) where T : Message, new();
        /// <summary>
        /// 订阅
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <returns></returns>
        bool Subscribe<T>(T message) where T : Message, new();

        bool Subscribe<T>(T message, int overdueTime) where T : Message, new();

        bool Connect();
    }
}
