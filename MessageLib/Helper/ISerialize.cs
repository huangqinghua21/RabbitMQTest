﻿using System.Text;

namespace MessageLib.Helper
{


    public interface ISerialize
    {
        T Deserialize<T>(byte[] content) where T : class;
        byte[] Serializer<T>(T serialObject) where T : class;

        T Deserialize<T>(string content) where T : class;
        string SerializerStr<T>(T serialObject) where T : class;
    }
}
