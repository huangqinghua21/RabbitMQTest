﻿using System.Text;
using MessageLib.Helper;

namespace Helper
{
    public class MqttSerializeJson : ISerialize
    {
        public byte[] Serializer<T>(T serialObject) where T : class
        {
            return Encoding.UTF8.GetBytes((serialObject.ToJson()));
        }

        public T Deserialize<T>(string content) where T : class
        {
            return content.ToObject<T>();
        }

        public string SerializerStr<T>(T serialObject) where T : class
        {
            return serialObject.ToJson();
        }

        public T Deserialize<T>(byte[] content) where T : class
        {
            string json = Encoding.UTF8.GetString(content);
            return json.ToObject<T>();
        }
    }


}
