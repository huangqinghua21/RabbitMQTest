﻿namespace Server
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.no_txt = new System.Windows.Forms.TextBox();
            this.subscribe_btn = new System.Windows.Forms.Button();
            this.push_btn = new System.Windows.Forms.Button();
            this.ckbShow = new System.Windows.Forms.CheckBox();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // no_txt
            // 
            this.no_txt.Location = new System.Drawing.Point(196, 15);
            this.no_txt.Name = "no_txt";
            this.no_txt.Size = new System.Drawing.Size(116, 21);
            this.no_txt.TabIndex = 29;
            this.no_txt.Text = "01";
            // 
            // subscribe_btn
            // 
            this.subscribe_btn.Location = new System.Drawing.Point(468, 12);
            this.subscribe_btn.Name = "subscribe_btn";
            this.subscribe_btn.Size = new System.Drawing.Size(75, 23);
            this.subscribe_btn.TabIndex = 28;
            this.subscribe_btn.Text = "订阅";
            this.subscribe_btn.UseVisualStyleBackColor = true;
            this.subscribe_btn.Click += new System.EventHandler(this.subscribe_btn_Click);
            // 
            // push_btn
            // 
            this.push_btn.Location = new System.Drawing.Point(570, 12);
            this.push_btn.Name = "push_btn";
            this.push_btn.Size = new System.Drawing.Size(75, 23);
            this.push_btn.TabIndex = 27;
            this.push_btn.Text = "推送";
            this.push_btn.UseVisualStyleBackColor = true;
            this.push_btn.Click += new System.EventHandler(this.push_btn_Click);
            // 
            // ckbShow
            // 
            this.ckbShow.AutoSize = true;
            this.ckbShow.Checked = true;
            this.ckbShow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbShow.Location = new System.Drawing.Point(21, 22);
            this.ckbShow.Name = "ckbShow";
            this.ckbShow.Size = new System.Drawing.Size(72, 16);
            this.ckbShow.TabIndex = 26;
            this.ckbShow.Text = "显示消息";
            this.ckbShow.UseVisualStyleBackColor = true;
            // 
            // txtInfo
            // 
            this.txtInfo.Location = new System.Drawing.Point(21, 47);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtInfo.Size = new System.Drawing.Size(624, 322);
            this.txtInfo.TabIndex = 25;
            this.txtInfo.WordWrap = false;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(21, 377);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 24;
            this.btnClear.Text = "清除所有";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(707, 439);
            this.Controls.Add(this.no_txt);
            this.Controls.Add(this.subscribe_btn);
            this.Controls.Add(this.push_btn);
            this.Controls.Add(this.ckbShow);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.btnClear);
            this.Name = "Form1";
            this.Text = "Server";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox no_txt;
        private System.Windows.Forms.Button subscribe_btn;
        private System.Windows.Forms.Button push_btn;
        private System.Windows.Forms.CheckBox ckbShow;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.Button btnClear;
    }
}

